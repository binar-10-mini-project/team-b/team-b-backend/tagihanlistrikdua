import {combineReducers} from 'redux';
import {GlobalReducer} from './GlobalReducer';
import {PrabayarReducer} from '../../src/Home/Redux/reducer';

export const AllReducer = combineReducers({
  GlobalReducer,
  PrabayarReducer,
});
