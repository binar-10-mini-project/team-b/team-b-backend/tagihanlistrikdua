import {all} from 'redux-saga/effects';
import {SagasPrabayar} from '../../src/Home/Redux/saga';

export function* SagaWatcher() {
  yield all([SagasPrabayar()]);
}
