import {all, takeLatest, put, call} from 'redux-saga/effects';
import axios from 'axios';
import {navigate} from '../../utils/Nav';
// import {navigate} from '../../../Utils/Nav';
// import jwt from 'jwt-decode';
// import {Alert} from 'react-native';

import {setLoading} from '../../Store/GlobalAction';
import InquiryPascabayar from '../../InquiryPascabayar';

function* SagaPrabayar(data) {
  try {
    yield put(setLoading(true));

    const Response = yield axios.get(
      `https://sandbox.centralbiller.co.id/product/sub?product_code=PLN-PREPAID`,

      {
        headers: {
          Token: `ZG0tcndiekFFWDJWaVVHTXFvdVA0Y1k`,
        },
      },
    );

    // // const Response = yield axios({
    // //   method: 'get',
    // //   url: `https://jsonplaceholder.typicode.com/users`,
    // //   // headers: {
    // //   //   Token: `ZG0tcndiekFFWDJWaVVHTXFvdVA0Y1k`,
    // //   // },
    // // });

    console.log(Response, 'hasil response');

    yield put({
      type: 'SET_PRABAYAR',
      nomor: Response.data.data,
    });
    // yield put(navigate('Inquiry', {}));
  } catch (error) {
    console.log(error);
  } finally {
    yield put(setLoading(false));
  }
}

function* SagaInquiry(data) {
  try {
    const body = {
      product_code: data.kodeproduk,
      customer_number: data.nomorpelanggan,
      additional: null,
    };
    const Response = yield axios.post(
      `https://sandbox.centralbiller.co.id/transaction/inquiry`,
      body,
      {
        headers: {
          Token: `ZG0tcndiekFFWDJWaVVHTXFvdVA0Y1k`,
        },
      },
    );

    console.log(Response, 'hasil response inquiry');

    yield put({
      type: 'SET_INQUIRY',
      kode: data.kodeproduk,
      nomor: data.nomorpelanggan,
      respon: Response.data.data,
    });

    yield put(
      navigate(
        `${
          data.kodeproduk === 'PLN-POSTPAID' ? 'InquiryPascabayar' : 'Inquiry'
        }`,
        {},
      ),
    );
  } catch (error) {
    console.log(error);
  }
  //   finally {
  //     yield put(setLoading(false));
  //   }
}

function* SagaPayment(data) {
  try {
    const body = {
      product_code: data.code,
      customer_number: data.custnumber,
      payment_reference: data.paymentref,
      additional: null,
    };
    const Response = yield axios.post(
      `https://sandbox.centralbiller.co.id/transaction/payment`,
      body,
      {
        headers: {
          Token: `ZG0tcndiekFFWDJWaVVHTXFvdVA0Y1k`,
        },
      },
    );

    console.log(Response, 'hasil response payment wey');

    // yield put({
    //   type: 'SET_INQUIRY',
    //   kode: data.kodeproduk,
    //   nomor: data.nomorpelanggan,
    //   respon: Response.data.data,
    // });
    yield put(navigate('SuccessPage', {}));
  } catch (error) {
    console.log(error);
  }
  //   finally {
  //     yield put(setLoading(false));
  //   }
}

export function* SagasPrabayar() {
  yield takeLatest('GET_PRABAYAR', SagaPrabayar);
  yield takeLatest('POST_INQUIRY_REQUEST', SagaInquiry);
  yield takeLatest('POST_PAYMENT', SagaPayment);
}
