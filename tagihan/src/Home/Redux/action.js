export const ActionPrabayar = nomor => {
  return {
    type: 'GET_PRABAYAR',
    nomor,
  };
};

export const ActionInquiryRequest = (kodeproduk, nomorpelanggan) => {
  return {
    type: 'POST_INQUIRY_REQUEST',
    kodeproduk,
    nomorpelanggan,
  };
};

export const ActionPayment = (code, custnumber, paymentref) => {
  return {
    type: 'POST_PAYMENT',
    code,
    custnumber,
    paymentref,
  };
};
