import React from 'react';
import {View, Text, StyleSheet, ImageBackground} from 'react-native';
import {connect} from 'react-redux';
import {background} from '../assets';

const SuccessPage = props => {
  return (
    <ImageBackground source={background} style={styles.back}>
      <View style={styles.sukses}>
        <Text style={styles.berhasil}>Pembayaran berhasil!</Text>
        <Text style={styles.terimakasih}>terimakasih {props.namacustomer}</Text>
      </View>
    </ImageBackground>
  );
};

const mapStateToProps = state => ({
  namacustomer: state.PrabayarReducer.respon.detail.customer_name,
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(SuccessPage);

const styles = StyleSheet.create({
  sukses: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  berhasil: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'white',
  },
  back: {
    flex: 1,
  },
  terimakasih: {
    color: 'white',
  },
});
