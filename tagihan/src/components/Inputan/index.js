import React from 'react';
import {View, Text, TextInput, StyleSheet} from 'react-native';
import Gap from '../Gap';

const Inputan = ({onChangeText, value}) => {
  return (
    <View style={styles.luar}>
      <Text style={styles.tulisan}>Masukkan nomor pelanggan: </Text>
      <Gap height={10} />
      <TextInput
        style={styles.kotak}
        onChangeText={onChangeText}
        value={value}
      />
    </View>
  );
};

export default Inputan;

const styles = StyleSheet.create({
  kotak: {
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 10,
    padding: 12,
  },
  luar: {
    padding: 10,
    alignSelf: 'center',
  },
  tulisan: {
    fontSize: 15,
    fontWeight: 'bold',
  },
});
