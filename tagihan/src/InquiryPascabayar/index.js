import React from 'react';
import {View, Text, StyleSheet, ImageBackground} from 'react-native';
import Button from '../components/Button';
import Gap from '../components/Gap';
import Loading from '../components/Loading';
import {connect} from 'react-redux';
import {backgroundinquiry} from '../assets';

const InquiryPascabayar = props => {
  console.log(props, 'ini props yea');

  // const kodepilihan = props.route.params.item.code;

  return (
    <ImageBackground source={backgroundinquiry} style={styles.back}>
      <View>
        <View style={styles.container}>
          <View style={styles.detailPageContainer}>
            {/* <Text style={styles.productNames}>{kodepilihan}</Text> */}

            <Text style={styles.productNames}>{props.respon.product_name}</Text>
            <Gap height={25} />

            <Text style={styles.productName}>Nama : </Text>
            <Text style={styles.jenis}>
              {props.respon.detail.customer_name}
            </Text>
            <Gap height={30} />

            <Text style={styles.bold}>Customer Meter : </Text>
            <Text>{props.respon.detail.customer_meter}</Text>

            <Gap height={15} />
            <Text style={styles.bold}>Customer Segmentation :</Text>
            <Text>{props.respon.detail.customer_segmentation}</Text>
            <Gap height={15} />

            <Text style={styles.bold}>Amount :</Text>
            <Text style={styles.harga}>
              Rp. {props.respon.detail.bills[0].amount}
            </Text>
          </View>
        </View>
        <Gap height={600} />
        <Button
          title="Checkout"
          onPress={() => {
            props.navigation.navigate('SuccessPage');
          }}

          // onPress={() => props.ActionInquiryRequest(kode, nomor)}
        />
      </View>
    </ImageBackground>
  );
};

const mapStateToProps = state => ({
  respon: state.PrabayarReducer.respon,
  produk: state.PrabayarReducer.nomor,
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(InquiryPascabayar);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: '#ffffff',

    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  detailPageContainer: {
    position: 'absolute',
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    width: 279,
    height: 322,
    // borderWidth: 2,
    borderRadius: 3,
    // borderColor: '#ddd',
    // borderBottomWidth: 0,
    // shadowColor: '#000',
    // shadowOffset: {width: 0, height: 1},
    // shadowOpacity: 1,
    // shadowRadius: 2,
    elevation: 2,
    top: 120,
  },
  productName: {
    fontWeight: 'bold',
    fontSize: 15,
  },
  productNames: {
    fontWeight: 'bold',
    fontSize: 20,
  },
  bold: {
    fontWeight: 'bold',
  },
  jenis: {
    fontWeight: 'bold',
    fontSize: 18,
  },
  harga: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  back: {
    flex: 1,
  },
});
