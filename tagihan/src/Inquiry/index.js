import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import Button from '../components/Button';
import Gap from '../components/Gap';

import Loading from '../components/Loading';

const Inquiry = props => {
  return (
    <>
      <View>
        <View style={styles.container}>
          <View style={styles.detailPageContainer}>
            <Text style={styles.productName}>{props.respon}</Text>
            <Text style={styles.jenis}>Pulsa Telkomsel 40 Ribu</Text>
            <Gap height={30} />

            <Text style={styles.bold}>Payment Reference :</Text>
            <Text>{props.noRef}</Text>
            <Gap height={15} />
            <Text style={styles.bold}>Phone Number :</Text>
            <Text>08221923123123</Text>
            <Gap height={15} />

            <Text style={styles.bold}>Amount :</Text>
            <Text style={styles.harga}>{props.total}</Text>
          </View>
        </View>
        <Gap height={600} />
        <Button
          title="Checkout"
          onPress={() => {
            props.navigation.navigate('SuccessPage');
          }}
        />
      </View>
    </>
  );
};

const mapStateToProps = state => ({
  respon: state.PrabayarReducer.kodeproduk,
  noRef: state.PrabayarReducer.respon.payment_reference,
  total: state.PrabayarReducer.respon.grand_total,
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(Inquiry);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  detailPageContainer: {
    position: 'absolute',
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    width: 279,
    height: 322,
    // borderWidth: 2,
    borderRadius: 3,
    // borderColor: '#ddd',
    // borderBottomWidth: 0,
    // shadowColor: '#000',
    // shadowOffset: {width: 0, height: 1},
    // shadowOpacity: 1,
    // shadowRadius: 2,
    elevation: 2,
    top: 120,
  },
  productName: {
    fontWeight: 'bold',
    fontSize: 25,
  },
  bold: {
    fontWeight: 'bold',
  },
  jenis: {
    fontWeight: 'bold',
    fontSize: 15,
  },
  harga: {
    fontSize: 20,
    fontWeight: 'bold',
  },
});
